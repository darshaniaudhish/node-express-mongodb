const Rect = require('./Rectangle');

Rect(12,13,(error,Rectangle) => {
    if(error){
        console.log(error.message);
    }else{
        console.log("Area of rectangle is " + Rectangle.area);
        console.log("Perimeter of rectangle is " + Rectangle.perimeter());
    }
})