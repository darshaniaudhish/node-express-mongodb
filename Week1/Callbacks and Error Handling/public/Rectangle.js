module.exports = (length,breadth,callback) => {
    var areaVal = function area(length,breadth){
        return length*breadth;
    }
    if(length !=0 && breadth != 0){
        callback(null,{
            area: areaVal(length,breadth),
            perimeter: () => (2 * (length + breadth))
        })
    }else{
        callback(new Error("Length/Breadth of the rectangle is zero"+ 
        "\n"+ "Length = "+ length + 
        "\n" + "Breadth = " + breadth),
        null);
    }
}