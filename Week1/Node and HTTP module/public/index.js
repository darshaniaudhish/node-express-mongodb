const Http = require('http');

var port = 8080;
var host = 'localhost';

const server = Http.createServer((req,res) => {
    console.log(req.headers);
    res.statusCode = 200;
    res.setHeader('Content-Type','Text/html');
    res.end('<html><body>Hello World!</body></html>')
});

server.listen(port,host,() => {
    console.log(`Server listening on port ${port}`);
});