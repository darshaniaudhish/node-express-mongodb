const http = require('http');
const path = require('path');
const fs = require('fs');

var host = 'localhost';
var port = 8080;

const server = http.createServer((req, res) => {
    var fileUrl = null;
    var file = null;
    if(req.method == 'GET'){
        if(req.url == "/"){
            fileUrl = "Index.html";
        }else{
            fileUrl = req.url;
            var filePath = path.resolve("./" + fileUrl);
            filePath = filePath + ".html";               
            if(fs.exists(filePath,(exists)=>{
                if(exists){
                    res.statusCode = 200;
                    res.setHeader("Content-Type","text/html");
                    fs.createReadStream(filePath,"utf8").pipe(res);
                }else{                    
                    res.statusCode = 404;
                    res.setHeader("Content-Type","text/html");
                    res.end('<html><body><h1>Error 404: ' + fileUrl + 
                    ' not a HTML file</h1></body></html>');                    
                }
            })){

            }            
    }    
    }else{
        res.statusCode = 404;
        res.setHeader("Content-Type","text/html");
        res.end('<html><body><h1>Error 404: ' + req.method + 
        ' not allowed</h1></body></html>'); 
        
    }
});

server.listen(port,host);