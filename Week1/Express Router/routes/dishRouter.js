const express = require('express');
const bodyParser = require('body-parser');
const dishRouter = express.Router();

dishRouter.use(bodyParser.json());

dishRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type","text/plain");
    next();
})
.get((req,res,next) => {
    res.end('Will send all the dishes to you!');
})
.post((req, res, next) => {
    res.end('Will add the dish: ' + req.body.name );
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /dishes');
})
.delete((req, res, next) => {
    res.end('Deleting all dishes');
});

dishRouter.get('/:dishId',(req,res,next) => {
    res.end('Getting dish '+ req.params.dishId);
})
.post('/:dishId',(req,res,next) => {
    res.status(404).send('POST method not supported for /dishes/'+req.params.dishId);
})
.put('/:dishId',(req,res,next) => {
    res.send('Updating dish ' + req.params.dishId + " with " + req.body.name);
})
.delete('/:dishId', (req,res,next) => {
    res.send('Deleting dish '+ req.params.dishId);
});

module.exports = dishRouter;