const express = require('express');
const bodyParser = require('body-parser');

const leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type","text/plain");
    next();
})
.get((req,res,next) => {
    res.send("Getting all the leaders");
})
.post((req,res,next) => {
    res.status(201).send("Adding leader " + req.body.name);
})
.put((req,res,next) => {
    res.status(405).send(req.method + " method not allowed");
})
.delete((req,res,next) =>{
    res.send("Deleting all the leaders");
});

leaderRouter.route('/:leaderId')
.get((req,res,next) => {
    res.send('Getting leader '+ req.params.leaderId);
})
.post((req,res,next) => {
    res.status(405).send(req.method + " method not allowed");
})
.put((req,res,next) => {
    res.send(req.params.leaderId + " leader updated with " + req.body.name);
})
.delete((req,res,next) => {
    res.send(req.params.leaderId + " successfully deleted");
});

module.exports = leaderRouter;