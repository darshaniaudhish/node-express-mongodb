const express = require('express');
const bodyParser = require('body-parser');

const promoRouter = express.Router();

promoRouter.use(bodyParser.json());

promoRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type","text/plain");
    next();
})
.get((req,res,next) => {
    res.send("Getting all the promo");
})
.post((req,res,next) => {
    res.status(201).send("Adding promo " + req.body.name);
})
.put((req,res,next) => {
    res.status(405).send(req.method + " method not allowed");
})
.delete((req,res,next) =>{
    res.send("Deleting all the promos");
});

promoRouter.route('/:promoId')
.get((req,res,next) => {
    res.send('Getting promo '+ req.params.promoId);
})
.post((req,res,next) => {
    res.status(405).send(req.method + " method not allowed");
})
.put((req,res,next) => {
    res.send(req.params.promoId + " promo updated with " + req.body.name);
})
.delete((req,res,next) => {
    res.send(req.params.promoId + " successfully deleted");
});

module.exports = promoRouter;