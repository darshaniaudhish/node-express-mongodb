const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = 8080;
const host = 'localhost';

app.use(bodyParser.json());

const dishRouter = require('./routes/dishRouter');
const leaderRouter = require('./routes/leaderRouter');
const promoRouter = require('./routes/promotionRouter');

app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promos', promoRouter);

const server = http.createServer(app);

server.listen(port,host,() =>{
    console.log(`Server listening at ${host}:${port}`);
})

