const express = require('express');
const http = require('http');
const morgan = require('morgan');
const path = require('path');

const host = 'localhost';
const port = 8080;
const app = express();

app.use(morgan('dev'));
var options = {
    dotfiles: 'ignore',
    etag: false,
    extensions: ['htm', 'html'],
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
      res.set('x-timestamp', Date.now())
    }
  }

app.use(express.static(path.join(__dirname + 'public'),options));

app.use('/AboutUs', express.static(path.join(__dirname + 'public'),options));
app.use('/Index', express.static(path.join(__dirname + 'public'),options));

app.use((req,res,next) => {    
    res.statusCode = 200;
    res.setHeader('Content-Type','text/html');
    res.end('<html><body><h1>Express server</h1></body></html>');
});



const server = http.createServer(app);
server.listen(port,host,() => {
    console.log(`Server listening at http://${host}:${port}`);
});
