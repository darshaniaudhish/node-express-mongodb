const mongoose = require('mongoose');
const Dishes = require('./models/dishes');

const url = 'mongodb://localhost:27017/conFusion';
mongoose.connect(url)
.then((db) => {
    console.log('Connected correctly to server');

    Dishes.create({
        name:"Palak Paneer",
        description: "Green curry"
    })
    .then((dish) => {
        console.log('dish created : ',dish);
        return Dishes.findByIdAndUpdate(dish._id,
                {$set: 
                    {
                        description:"appetizing curry with fresh paneer and palak"}
                },
                    {
                        new: true
                    }                
                ).exec();
    })
    .then((dish) => {
        console.log('Dish found : ',dish);
        
        dish.comments.push({
            rating: 5,
            comment: "amazing dish",
            author: "Darshani"
        });

        return dish.save();

    })
    .then((dish)=>{
        console.log('dish updated with comment ',dish);
        return Dishes.remove({});
        
    })
    .then(()=>{
        return db.connection.close();
    })
    .then(()=>{
        console.log('Connection to db closed successfully');
    })
    .catch((error)=>{
        console.log(error);
    });


})
.catch((error) => console.log(error))
