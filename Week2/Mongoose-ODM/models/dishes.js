const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
    rating:{
        type:Number,
        required: true,
        min : 1,
        max : 5
    },
    comment:{
        type: String        
    },
    author:{
        type: String,
        unique: true
    }
},{
    timestamps: true
});

const dishSchema = new Schema({
    name:{
        type : String,
        required : true,
        unique: true
    },
    description:{
        type: String,
        required: true        
    },
    comments: [commentSchema]
},{
    timestamps: true
});

var Dishes = mongoose.model("dish",dishSchema);
module.exports = Dishes;
