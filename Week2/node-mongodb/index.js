const mongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const ops = require('./operations');

const url = 'mongodb://localhost:27017/';
const dbname = 'conFusion';

mongoClient.connect(url,(err, client) => {
    
    assert.equal(err,null);
    console.log("Connected to database");    

    const db = client.db(dbname);   
    ops.insertDocument(db,"dishes",
    {"name":"Paneer Paratha","description":"Green curry"},
    (result) => {
        console.log(`${result.insertedCount} document inserted`);
        ops.findDocument(db,"dishes",(documents) => {
            console.log(documents[0] + ' found');            
            console.log('Before updating the document');
            ops.updateDocument(db,"dishes",documents[0],
                {"description": "Appetizing curry with fresh paneer and green palak"},(result) => {
                    console.log(`${result.modifiedCount} document updated`);
                    console.log('Before deleting the found document');
                    if(documents.length == 1){
                        ops.deleteDocument(db,"dishes",documents[0],(result) => {
                            console.log(result.n + ' document deleted');
                            db.dropCollection("dishes", (result) => {
                                console.log("Dropped Collection: ", result);

                                client.close();
                            });                               

                        }); 
                    } 
            });
        });
    });
    
});