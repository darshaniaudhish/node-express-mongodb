exports.findDocument = (db,collection) => {
    const coll = db.collection(collection);
    return coll.find({}).toArray();
}

exports.insertDocument = (db,collection,document) => {
    const coll = db.collection(collection);
    return coll.insertOne(document);
}

exports.updateDocument = (db,collection,document,update) => {
    const coll = db.collection(collection);
    return coll.updateOne(document,{$set:update},null);
}

exports.deleteDocument = (db,collection,document) => {
    const coll = db.collection(collection);
    return coll.deleteOne(document);
}



