const MongoClient = require('mongodb').MongoClient;
const dboper = require('./operations_promises');

const url = 'mongodb://localhost:27017/';
const dbname = 'conFusion';

var document = {"name":"Paneer Paratha","description":"Green curry"};
var update = {"description" : "Appetizing curry with fresh paneer and green palak"};
var collection = "dishes";

/**
MongoClient.connect(url).then((client) => {

    console.log('Connected correctly to server');
    const db = client.db(dbname);

    dboper.insertDocument(db,collection, document)
        .then((result) => {
            console.log("Insert Document:\n", result.dboper);

            return dboper.findDocument(db, collection);
        })
        .then((docs) => {
            console.log("Found Documents:\n", docs);

            return dboper.updateDocument(db, collection, document,
                    update);

        })
        .then((result) => {
            console.log("Updated Document:\n", result.result);

            return dboper.findDocument(db, collection);
        })
        .then((docs) => {
            console.log("Found Updated Documents:\n", docs);
                            
            return dboper.deleteDocument(db,collection,document);            
        })
        .then((result) => {
            console.log("Deleted document \n",result.result);

            return db.dropCollection("dishes");
        })
        .then((result) => {
            console.log("Dropped Collection: ", result);

            return client.close();
        })
        .catch((err) => console.log(err));

})
.catch((err) => console.log(err));
*/

MongoClient.connect(url).then((client) => {    
        
        console.log("Connected to database");    

        const db = client.db(dbname);  
        dboper.insertDocument(db,collection,document)
            .then((result) => {
                console.log(result.dboper , ' inserted');
                return dboper.findDocument(db,collection);
            })
            .then((result) => {
                console.log('found document ',result.dboper);
                return dboper.updateDocument(db,collection,document,update);
            })
            .then((result) => {
                console.log('updated document ',result.dboper);
                return dboper.deleteDocument(db,collection,document);
            })
            .then((result)=>{
                console.log('deleted document : ' , result.dboper);
                return db.dropCollection(collection);
            })
            .then((result) => {
                console.log("Dropped Collection: ", result);
                return client.close();
            })
            .catch((error) => {console.log(error)});
   
    })
    .catch((error) => {
        console.log(error)
    });

    