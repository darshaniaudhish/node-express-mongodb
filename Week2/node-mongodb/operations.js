const assert = require('assert');

exports.findDocument = (db,collection,callback) => {
    const coll = db.collection(collection);
    coll.find({}).toArray((error,documents) => {
        assert.equal(error,null);
        console.log(`${documents.length} documents found`);
        callback(documents);
    });
}

exports.insertDocument = (db,collection,document,callback) => {
    const coll = db.collection(collection);
    coll.insertOne(document,(error,result) => {
        assert.equal(error,null);
        console.log('document' + document + 'inserted');
        callback(result);
    })
}

exports.updateDocument = (db,collection,document,update,callback) => {
    const coll = db.collection(collection);
    coll.updateOne(document,{$set:update},(error,result) => {
        assert.equal(error,null);
        console.log('document' + document + ' updated with ' + update);
        callback(result);
    });
}

exports.deleteDocument = (db,collection,document,callback) => {
    const coll = db.collection(collection);
    coll.deleteOne(document,(error,result) => {
        assert.equal(error,null);
        console.log('document' + document + 'deleted');
        callback(result);
    });
}



